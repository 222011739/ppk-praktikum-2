> Nama : Muhammad Naufal Faishal            
Kelas / Absen : 3SI3 / 27

## Laporan Praktikum 2

1. Tangkapan Layar halaman cd_katalog.xml
    1. Tampilan pada browser
        >![katalogBrowser](/image/browserKatalog.png "katalog")
    2. Validator
        >![katalogBrowser](/image/validateKatalog.png "katalog")
1. Tangkapan Layar simple.xml
    1. Tampilan pada browser
        >![katalogBrowser](/image/browserSimple.png "simple")
    2. Validator
        >![katalogBrowser](/image/validateSimple.png "simple")
1. Tangkapan Layar note.xml
    1. Tampilan pada browser
        >![katalogBrowser](/image/browserNote.png "note")
    2. Validator
        >![katalogBrowser](/image/validateNote.png "note")


